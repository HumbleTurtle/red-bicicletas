# Uso
    git clone https://HumbleTurtle@bitbucket.org/HumbleTurtle/red-bicicletas.git

    npm run devstart

# Endpoints

    GET     /api/bicicletas

    POST    /api/bicicletas/create
            Para postman usar body con formato x-www-form-urlencoded

            Campos esperados
                id : id bicicleta
                color  : color de la bicicleta
                modelo : modelo de la bicicleta
                lng : longitud de la coordenada
                lat : latitud de la coordenada


    POST    /api/bicicletas/:id/update
            Enviar datos como params en postman

            Campos esperados
                id : id bicicleta

    POST    /api/bicicletas/:id/delete
            Enviar datos como params en postman

            Campos esperados
                id : id bicicleta
