var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

// Registramos las rutas en otro archivo para tener mejor orden en el app.js
// Dividimos las rutas en dos archivos router
// routes/routesAPI.js <- Contiene las rutas del api
// routes/routesLocal.js <- Contiene las rutas del api
// routes/routes.js <- Importa ambos archivos
var routes = require('./routes/routes.js')

var app = express();

// Instalamos el view manager
instalar_vistas( app );

// Configuramos el app
configurar_app( app );

// Registramos las rutas
routes.register(app);

// Registramos el manejador de errores
instalar_manejador_errores(app);


function instalar_manejador_errores(app) {
 
  // catch 404 and forward to error handler
  app.use(function(req, res, next) {
    next(createError(404));
  });

  // error handler
  app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
  });
} 

function instalar_vistas (app) {
  // view engine setup
  app.set('views', path.join(__dirname, 'mvc', 'views'));
  app.set('view engine', 'pug');
}

function configurar_app (app){
  app.use(logger('dev'));
  app.use(express.json());
  app.use(express.urlencoded({ extended: false }));
  app.use(cookieParser());
}

module.exports = app;
