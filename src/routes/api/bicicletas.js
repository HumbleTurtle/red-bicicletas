var express = require('express');
var router = express.Router();

var bicicletaController = require('../../mvc/controllers/api/bicicletaControllerAPI');

router.get('/', bicicletaController.bicicleta_list );
router.post('/create', bicicletaController.bicicleta_create );

router.post('/:id/update', bicicletaController.bicicleta_update );
router.put('/:id/update', bicicletaController.bicicleta_update );

/* Post y delete para las pruebas */
router.post('/:id/delete', bicicletaController.bicicleta_delete );
router.delete('/:id/delete', bicicletaController.bicicleta_delete );

module.exports = router;