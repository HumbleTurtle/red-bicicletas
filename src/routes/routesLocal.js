var express = require('express');
var path = require('path');

// Subrutas
var indexRouter = require('./local/index');
var usersRouter = require('./local/users');
var bicicletasRouter = require('./local/bicicletas');

var router = express.Router();

// Mapeamos las rutas con subrutas
router.use('/', indexRouter);
router.use('/users', usersRouter);
router.use('/bicicletas', bicicletasRouter);

module.exports = router;