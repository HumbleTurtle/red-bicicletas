var Bicicleta = function( id, color, modelo, ubicacion ) {
    this.id = id;
    this.color = color;
    this.modelo = modelo;
    this.ubicacion = ubicacion;
}

Bicicleta.prototype.toString = function () {
    return 'id: ' + this.id + " | color: " + this.color;
}

Bicicleta.allBicis = [];

Bicicleta.add = function( aBici ) {
    Bicicleta.allBicis.push(aBici);
}

Bicicleta.findById = function(aBiciId) {
    var aBici = Bicicleta.allBicis.find( x=> x.id == aBiciId );

    if(aBici)
        return aBici;
    else
        throw new Error(`No existe una bicicleta con el id `+aBiciId );
}

Bicicleta.removeById = function( aBiciId ) {
    var encontrada = false;

    for( var i =0 ; i < this.allBicis.length ; i++ ) {
        if( this.allBicis[i].id == aBiciId ) {
            this.allBicis.splice(i,1)
            encontrada = true;
        }
    }

    if(!encontrada)
        throw new Error(`No existe una bicicleta con el id `+aBiciId );
}

/**
 *  var marker = L.marker([51.5, -0.09]).addTo(mymap);
    var marker = L.marker([51.49, -0.09]).addTo(mymap);
    var marker = L.marker([51.51, -0.09]).addTo(mymap);
    var marker = L.marker([51.51, -0.08]).addTo(mymap);
*/

Bicicleta.add( new Bicicleta(1, "rojo", "urbana", [51.5, -0.09] ) );
Bicicleta.add( new Bicicleta(2, "azul", "montaña", [51.49, -0.09] ) );
Bicicleta.add( new Bicicleta(3, "gris", "urbana", [51.51, -0.08] ) );


module.exports = Bicicleta