var Bicicleta = require("../../models/bicicleta");

exports.bicicleta_list = function(req, res) {
    res.status(200).json({
        bicicletas: Bicicleta.allBicis
    });
}

exports.bicicleta_create = function(req,res) {
    var body = req.body;
    
    var ubicacion = [body.lat,body.lng];
    var bici = new Bicicleta( body.id, body.color, body.modelo, ubicacion );

    Bicicleta.add(bici);

    res.status(200).json({
        bicicleta: bici
    });
}

exports.bicicleta_update = function(req, res) {
    var bici;
    
    // Si no encontramos la bicicleta
    try {
        bici = Bicicleta.findById(req.params.id);
    } catch(err) {
        
        res.status(404).json({
            message: "Not found"
        });

        return;
    }

    bici.id = req.body.id;
    bici.color = req.body.color;
    bici.modelo = req.body.modelo;
    bici.ubicacion = [req.body.lat, req.body.lng];

    res.status(200).json({
        bicicleta: bici
    });
}

exports.bicicleta_delete = function(req,res) {
    var params = req.params;
    var id = parseInt( params.id );

    try {
        Bicicleta.removeById(id);
    } catch(err) {
        console.log(err);
    }
    // Delete es idempotente, siempre retorna 200 aunque ya se haya borrado el archivo
    res.status(200).json({Response:"Ok"});
}